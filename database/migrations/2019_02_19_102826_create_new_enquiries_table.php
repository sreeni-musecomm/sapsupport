<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewEnquiriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('new_enquiries', function (Blueprint $table) {
            $table->increments('id');
            $table->string('f_name',100);
            $table->string('l_name',100);
            $table->string('email',150);
            $table->tinyInteger('user_type');
            $table->string('typeofinfo',255);
            $table->string('certi_others',255)->default(NULL);
            $table->string('file_path',255);
            $table->text('message');
            $table->string('reply_status',255)->default(NULL);;
            $table->string('ticket_no',255);
            $table->string('ticket_url',255);
            $table->string('ip_address',255)->default(NULL);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('new_enquiries');
    }
}
