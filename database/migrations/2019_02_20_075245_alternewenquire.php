<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alternewenquire extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('new_enquiries', function (Blueprint $table) {
            $table->string('status',50)->after('message')->default(0);
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('new_enquiries', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
