<?php

return [

	'message_sent'                  => 'successfully sent',
	'emailId_mismatch'				=> 'Email Id and Password is wrong',
	'internal_server_error'			=> 'Internal Server Error',
	'message_forward'               => 'Successfully Forwarded',
	'message_forward_failer'		=> 'Please Check the smtp Details',
	'user_registered'               => 'Successfully Registered',
	'unauthorized' 					=> 'unauthorized',
	'reset_success'					=> 'Password reset successfully',
    'check_email'  					=> 'Please check your mail id'			
];