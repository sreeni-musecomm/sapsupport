
<!DOCTYPE html>
<html>
  <head>
    <title>404 Page Not Found</title>
    <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
      <link href="https://www.sapindiacertification.com/certification/asset/css/common.css" rel="stylesheet">
      <link rel="stylesheet" href="https://sapforgrowth.com/training/my/asset/css/fonts.css">
<style>
.navbar{min-height: 60px !important;}
.logo{padding-top: 5px !important;}
.main-content{
  margin-top:4%;
  margin-bottom: 12%;
}
h4
{
  font-size: 24px !important;
}
  .heading{
    margin-top: 30px
  }
  .sorry
  {
      text-align: center;
      padding-top: 90px;
      padding-bottom: 50px;
  }
  .may
  {
      text-align: center;
      padding-top: 30px;
      padding-bottom: 50px;
  }
  .sparator
  {
    background-color: #f0ab00;
    height: 5px;
  }
  ul li{
    display:inline;
    font-size: 14px
  }
  ul .pipe{
    padding-right: 10px;
    padding-left: 10px;
  }
  .navbar-brand {
      padding: 9px 15px 10px 15px !important;
    }
  @media only screen and (min-width:320px) and (max-width:767px) {
    .navbar{min-height: 50px !important;}
    .main-content{
      margin-top:15%;
      margin-bottom: 12%;
    } 
    .logo{padding-top: 0px !important;}
    .heading{font-size: 23px}
    h4{font-size: 16px !important}
    .sorry
    {
        padding-top: 30px;
        padding-bottom: 20px;
    }
    .may
    {
        padding-top: 10px;
        padding-bottom: 20px;
    }
    .navbar-brand {
      padding: 9px 15px 10px 15px !important;
    }
    .motionBand.primary{
      top:50px !important;
    }
    ul{text-align: center;padding-left: 0px !important;}
    ul li{
    display:block;
    list-style: none;
    font-size: 14px;
    padding-bottom: 10px;
  }
}
</style>
    <script src="https://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="https://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
     
  </head>
  <body>
    <nav class="navbar navbar-fixed-top navbar-inverse navbar-certification">
        <div class="container-fluid">
          <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
            <div class="col-xs-6 navbar-header p-l-r-0">
                <a class="navbar-brand" href="https://www.sapindiacertification.com/certification/"><img src="https://sapforgrowth.com/training/my/asset/images/sap-logo.png" class="img-responsive logo"></a>
            </div>
             <!-- Collect the nav links, forms, and other content for toggling -->
          </div>  
        </div><!-- /.container-fluid -->
    </nav>

<!-- <div class="container-fluid bg_ban xs-pad ">
  <div class="container">
    <div class="row">
          <div class="col-md-9 col-sm-9 col-xs-9">
            <h2 class="heading" style="color: #FFFFFF;">Page Not Found</h2>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-3"></div>
    </div>
    <div class="row">
          <div class="motionBand primary light2dark">
        <span class="part1"></span>
        <span class="part2"></span>
        <span class="part3"></span>
      </div>
      </div>
  </div>
</div> -->

<section class="body-text main-content">
  <div class="container">
    <div class="row p-l-r-0">
      <div class="col-md-12 sorry">
        <div>
           <h4>
            Sorry, this page can’t be found
          </h4>
          <p class="hidden">
            Please try searching  <a href="www.sapindiacertification.com">www.sapindiacertification.com</a> or browse the links below
          </p>
        </div>
      </div>
    </div>
    <div class="col-md-12">

      <div class="sparator">
      </div>
    </div>
        <div class="row">
        <div class="col-md-12 may">
          <div>
             <h4 style="padding-bottom: 10px">
             You may also be interested in these links
            </h4>
            <ul>
                <li>
                  <a href="https://sapforgrowth.com/enablenow/">
                    SAP Enable Now
                  </a>
                </li>
                <li class="pipe hidden-xs">|</li>
                <li>
                  <a href="https://sapforgrowth.com/skillsuniversity/">
                    SAP Skill University
                  </a>
                </li>
                <li class="pipe hidden-xs">|</li>
                <li>
                  <a href="https://sapforgrowth.com/training/">
                    SAP Classroom Training <span class="hidden-xs"> (Philippines/ Malaysia)</span>
                    <br>
                    <span class="visible-xs"> (Philippines/ Malaysia)</span>
                  </a>
                </li>
                <li class="pipe hidden-xs hidden">|</li>
                <li class="hidden">
                  <a href="https://training.sap.com/course/cer006-sap-certification-in-the-cloud-sap-certification-in-the-cloud-certification-online-010-in-en/">
                    SAP Certification in the Cloud
                  </a>
                </li>
            </ul> 
          </div>
      </div>
          
    </div>
  </div>
</section>
</body>
</html>