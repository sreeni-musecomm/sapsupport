@extends('user.layouts.app')
@section('title')
@parent 
Support Chat - thank you
@stop
@section('content')
<section class="body-text">
	<div class="container-fluid pad-t-b-0 mar-t-0 pad-l-r-0">
		<div class="container thankyou" style="padding-top:50px;">
			<div class="row">
				<div class="col-md-12 com-sm-12 col-xs-12 pad-l-5">
					<span class="thankyou_text">
						Thank you for your message. <br>We will get in touch with you soon.
					</span>
				</div>
				<div class="col-md-12 com-sm-12 col-xs-12 thk-btn">
					<span>
						<a href="https://www.sapindiacertification.com" class="btn btn-warning" style="font-size: 22px;">Home</a>
					</span>
				</div>
			</div>
		</div>
	</div>
	<div class="m-b-50"></div>
</section>
@stop

