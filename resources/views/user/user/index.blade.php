@extends('user.layouts.app')
@section('title')
@parent 
Support Chat
@stop 
@push('css_script')
<link href="{{ asset('assets/css/user_style.css') }}" rel="stylesheet">
@endpush
@section('content')

<style type="text/css">
    .invalid-feedback
    {
        color: #ef2d2d;
        margin-top:5px;
        display:inline-block;
    }
</style>
<section class="body-text">

<div class="container-fluid m-t-20">
	<div class="container">
		<!-- background-color: #e3e3e3; -->
		<div class="row" style="padding: 20px;background-color: #e3e3e3; border: 1px solid #c4c4c4;"> 
    		<div class="col-md-12 col-sm-12 col-xs-12 text-center">
    		<p class="p-t-5 text-cente">Please note, <a href="">support@sapindiacertification.com</a> is disabled.</p>
    		<p class="text-cente">All the queries related to SAP certification portal would be addressed by submitting the below enquiry form.</p>
    		</div>
    		<div class="clearfix"></div>
    	</div>
    	<div class="row">
    		<div class="col-md-12">
    			<h3 class="text-center"><br>Tell us about the issue you are facing, and we will get back to you as soon as possible.</h3>
				<div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12 col-xs-offset-0 p-l-0  p-t-20">
					
                    <div class="row text-center">
                        <div class="col-md-6" style="padding-left">
                            <div class="enquiry-tab new-enquiry active-tab">New enquiry</div>
                        </div>
                        <div class="col-md-6 ">
                            <div class="enquiry-tab exsisting-enquiry">Existing enquiry</div>
                        </div>

                        <div class="col-md-12 text-left" id="enquiry-form-div">
                            <form action="{{url('new-enquiry')}}" method="post" id="new-enquiry" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="row m-t-0">
                                    
                                    <div class="col-md-12">
                                        <input type="radio" name="user_type" @if(old('user_type') == 0) checked @endif class="user_new"  value="0" id="user_type" onclick="checkUser('user_new')">
                                        <label>New User </label>
                                        <input style="margin-left:30px;" type="radio" name="user_type" @if(old('user_type') == 1) checked @endif value="1" class="user_exist" id="user_type" onclick="checkUser('user_exist')">
                                        <label>Registered User</label>
                                    </div>
                                    
                                    <div class="col-md-12">
                                        @if ($errors->has('user_type'))
                                        <div>
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('user_type') }}</strong>
                                            </span>
                                        </div>
                                        @endif
                                        <div class="form_name">
                                            <div class="m-t-20">
                                                <input type="text" name="first_name" id="firstname" value="{{ old('first_name')}}" class="form-control" placeholder="First name*">
                                                @if ($errors->has('first_name'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('first_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>

                                            <div class="m-t-20">
                                                <input type="text" name="last_name" value="{{ old('last_name')}}" id="lastname" class="form-control" placeholder="Last name*">
                                                @if ($errors->has('last_name'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('last_name') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="m-t-20">
                                            <input type="email" name="email" value="{{ old('email')}}" id="email" class="form-control" placeholder="Enter your registered email ID*">
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="m-t-20">
                                            <select style="padding-left:6px;" name="typeofinfo" id="typeofinfo" class="form-control">
                                                <option value=" " class="certification_360">Select certification *</option>
                                                @foreach($typeOfInfo as $value)
                                                @if(old('typeofinfo') == $value)
                                                <option selected="selected" value="{{$value}}">{{ $value }}</option>
                                                @else
                                                <option value="{{$value}}">{{ $value }}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                            @if ($errors->has('typeofinfo'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('typeofinfo') }}</strong>
                                                </span>
                                            @endif
                                            <div class="m-t-20" style="display:none;" id="others">
                                                <input type="text" name="certi_others" value="{{ old('certi_others')}}" id="certi_others" class="form-control" placeholder="Please specify*">
                                                @if ($errors->has('certi_others'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('certi_others') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                    </div>


                                    <div class="col-md-12">
                                        <div class="m-t-20">
                                            <label class="m-b-10">Attach file:</label>
                                            <input type="file" id="file_path" name="file_path" onchange="return ValidateFileUpload('file_path')">
                                            <span class="m-t-5" style="color: #7e7e7e;font-size: 10px;">(File Format: .doc,.docx,pdf,.xlsx,.xls Max size: 2MB)</span>
                                            <div id="_file" class="hidden" style="color: red"><label id="error_msg_file"></label></div>
                                            @if ($errors->has('file_path'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('file_path') }}</strong>
                                                </span>
                                            @endif
                                            @if(Session::has('message'))
                                            <div>
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ Session::get('message') }}</strong>
                                                </span>
                                            </div>
                                            @endif  
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="m-t-20">
                                            <textarea class="form-control" id="editor" name="message" placeholder="Enter your message*" style="height: 100px">{{ old('message')}}</textarea>
                                            @if ($errors->has('message'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('message') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="m-t-b-20">
                                            <button type="submit" class="btn btn-warning btn-send" name="btn-send" value="submit">Submit</button>
                                        </div>
                                    </div>
                                    
                                </div>
                                
                                
                                
                                
                                
                                
                            </div>
                        </form>
                        <form style="min-height:400px;" action="{{url('ex-enquiry')}}" method="post" class="form-group hidden text-left" id="exis-enquiry" enctype="multipart/form-data">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="col-md-12">
                                <div class="m-t-20">
                                    <label class="text-left" for="firstname">Enter email ID used to register the complaint</label>
                                    <input type="email" name="email" id="firstname" value="{{ old('email')}}" class="form-control" placeholder="E-mail ID *">
                                    <span class="invalid-feedback email-error" role="alert">
                                            <strong></strong>
                                    </span>
                                </div>
                                <div class="row m-t-b-20">
                                    <div class="col-md-12"><button type="submit" class="btn btn-warning btn-send ticket-check" name="btn-send" value="submit">Submit</button></div>                                   
                                </div>
                                <div id="ticket-details" data-url="{{ url('ticket-view') }}"></div>

                            </div>
                        </form>
                        </div>

                    </div>
                    
                    
				</div>
                <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
                    
                </div>
    		</div>
			
    		<!-- Mobile View -->
	    	
		</div>
	</div>
</div>

</section>
@stop

@push('script')
<script src="{{ asset('assets/js/user/custom.js')}}"></script>
<script src="{{ asset('assets/js/user/file_validation.js')}}"></script>
@endpush