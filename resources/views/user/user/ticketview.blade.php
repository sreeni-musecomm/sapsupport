@extends('user.layouts.app')
@section('title')
@parent 
Support Reply	
@stop
@section('content')
<style type="text/css">
    .invalid-feedback{
        color: red;
	}
	
	body
	{
		background:#f5f5f5;
	}
</style>
<section class="body-text">
 @if(Session::has('message'))
               <div class="alert alert-danger fade in alert-dismissible" style="margin-top:18px;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                     {{  Session::get('message') }}
                </div>
                @endif 
<div class="container-fluid m-t-20">
	<div class="container">
		<!-- background-color: #e3e3e3; -->
    	<div class="row">
    		<div class="col-md-12 col-sm-8 col-xs-12" style="padding-left:0px;">
    			
					<div class="col-md-12" >
						@if(Auth::user())
						<h3 class="{{ Auth::user() ? 'text-left': 'text-center'}}" style="margin-bottom:30px;"><a class="go-back-link btn btn-warning pull-right" href="{{ url('dashboard')}}"><i class="fa fa-chevron-left"></i> Dashboard</a>{{ $ticket_details->typeofinfo }} | SAP India Certification | Support Ticket No : <label>{{ $ticket_details->ticket_no }}</h3>
						@else
						<h3 class="text-left" style="margin-bottom:30px;">{{ $ticket_details->typeofinfo }} | SAP India Certification | Support Ticket No : <label>{{ $ticket_details->ticket_no }}</label></h3>
						@endif
						<div class="user-reply">
							<div class="ticket-user-name"><span><b>{{ $ticket_details->f_name }} {{ $ticket_details->l_name }}</b></span></div>
							<div class="user-test">
								<p>{{ $ticket_details->message }}</p>
								@if($ticket_details->file_path)
								<div><a class="btn btn-warning btn-xs conv-download-btn" href="{{ url('public').'/support/attachment/'.$ticket_details->file_path }}" download="">Attachment</a></div>
								@endif
							</div>
							
							
							
							<div class="user-time-date"><span>{{$ticket_details->created_at->format('h:i')}} {{$ticket_details->created_at->format(' A')}}</span>, <span>{{ $ticket_details->created_at->format('D-M-Y') }}</span></div>
						</div>
						@if($ticket_details->conversations)
							@foreach($ticket_details->conversations as $value)
								<div class="{{ $value->created_by == 'admin'?'admin-reply':'user-reply'}}">
									<div class="ticket-{{ $value->created_by == 'admin'? 'admin':'user' }}-name"><span><b>{{ $value->created_by == 'admin'? 'Support':$ticket_details->f_name }}</b></span> <span><b>{{ $value->created_by == 'admin'? '':$ticket_details->l_name }}</b></span></div>
									<div class="{{ $value->created_by == 'admin'?'admin-test':'user-test'}}">
										<p>{!! $value->messages !!}</p>
										@if($value->file_path != 'null')
										<div><a class="btn btn-warning btn-xs conv-download-btn" href="{{ url('public').'/support/attachment/'.$value->file_path }}" download="">Attachment</a></div>
										@endif
									</div>
									
									<div class="{{ $value->created_by == 'admin'?'admin-time-date':'user-time-date'}}">
										<span>{{$value->created_at->format('h:i')}} {{$value->created_at->format(' A')}}</span>, <span>{{ $value->created_at->format('D-M-Y') }}</span>
										
									</div>

									

								</div>
							@endforeach
						@endif
						<!-- <div class="admin-reply">
							<div><span><b>Support</b></span></div>
							<div class="admin-test"><p>FDSSDFS</p></div>
							<div><a href="#" download="">Download</a></div>
							<div class="admin-time-date"><span>2:30pm</span> | <span>Thu aug 2019</span></div>
						</div> -->
						<div >
	    					<form action="{{url('reply-user')}}" method="post" class="form-group" id="new-enquiry" enctype="multipart/form-data">
	                    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
	                    	<input type="hidden" name="redirect_id" value="{{ $ticket_details->ticket_url }}">
		    					<div class="row m-t-20 p-l-r-15 ">
		    						<textarea class="form-control" id="{{ Auth::user() ? 'editor':''}}" name="message" placeholder="Enter your message*" style="height: 100px">{{ old('message')}}</textarea>
		                            @if ($errors->has('message'))
		                                <span class="invalid-feedback" role="alert">
		                                    <strong>{{ $errors->first('message') }}</strong>
		                                </span>
		                            @endif
		    					</div>
		    					<div class="row m-t-20 p-l-r-15">
		                        	<label class="m-b-10">Attach file:</label>
		                            <input type="file" id="file_path" name="file_path" onchange="return ValidateFileUpload('file_path')">
		                            <span class="m-t-5" style="color: #7e7e7e;font-size: 10px;">(File Format: .doc,.docx,pdf,.xlsx,.xls Max size: 2MB)</span>
		                            <div id="_file" class="hidden" style="color: red"><label id="error_msg_file"></label></div>
		                            @if ($errors->has('file_path'))
		                                <span class="invalid-feedback" role="alert">
		                                    <strong>{{ $errors->first('file_path') }}</strong>
		                                </span>
		                            @endif
		                        </div>
		    					<div class="row m-t-b-20 p-l-r-10">
		    						<button type="submit" class="btn btn-warning btn-send" name="btn-send" value="submit">Send</button>
		    					</div>
	    					</form>
    					</div>
    				</div>
    		</div>
		</div>
	</div>
</div>
</div>

</section>
@stop
@push('script')
<!-- Content ends -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script src="https://cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
<script type="text/javascript">
CKEDITOR.replace( 'editor' );
</script>
<script src="{{ asset('assets/js/user/file_validation.js')}}"></script>
@endpush