 <div class="container-fluid pad-t-b-15 back_footer  hidden-xs">
            <div class="container">
              <div class="row">
                
                <div class="col-md-12">
                  <div class="footer_txt">
                    <div class="col-md-5 col-sm-5 text-left p-l-0 social-icons-block">
                        <ul class="footer_ul">
                    
                                <li class="p-t-5">Find us on</li>
                                <li><a href="https://www.facebook.com/SAPEducation" target="_blank" class="si"><i class="fa fa-facebook-square"></i><a></li>
                                <li><a href="https://twitter.com/sapedu" target="_blank" class="si"><i class="fa fa-twitter-square"></i></a></li>														
                                <li><a href="https://www.linkedin.com/groups?gid=3918791" class="si"><i class="fa fa-linkedin-square"></i></a></li>
                                <li><a href="https://www.youtube.com/user/SAPEducation2011" class="si"><i class="fa fa-youtube-square"></i></a></li>
                                
                        </ul>
                    </div>
                    <div class="col-md-7 col-sm-7 float-right text-right" style="padding-right:0px;">
                              <ul class="footer_ul  list-inline footer-desk-top p-t-20">
                                <li><a target="_blank" href="http://www.sap.com">SAP</a></li>
                                <li><a target="_blank" href="https://www.sapindiacareer.com/legal/privacy">Privacy</a></li>
                                <li><a target="_blank" href="http://www.sap.com/corporate-en/about/legal/terms-of-use.html">Terms of Use</a></li>
                                <li><a target="_blank" href="http://www.sap.com/corporate-en/about/legal/impressum.html">Legal Disclosure</a></li>
                                <li><a target="_blank" href="http://www.sap.com/corporate-en/about/legal/copyright/index.html">Copyright / Trademark</a></li>
                                <li>&copy; {{date('Y')}} SAP SE</li>
                              </ul>
                    </div>
                  </div>
                </div>

                  
                  
                
              </div>

                
            
              </div>
            </div>
        </div>
    <!-- Mobile Links-->
  <div class="container-fluid hidden-lg hidden-md hidden-sm pad-t-b-0 back_footer">
    <div class="container">
      <div class="row p-t-30">
        <div class="col-md-5 text-left p-l-0 social-icons-block">
          <span class="footer_ul footer_ul_calender text-center headline gcolor">Share &amp; Follow</span>
            <ul class="footer_ul">
              <!-- <li class="p-t-10" style="font-size:14px;font-weight:bold;">Share & Follow</li> -->
                <li>
                    <a href="https://www.linkedin.com/groups?gid=3918791" target="_blank">
                     <img class="ftnew" src="https://www.sapindiacertification.com/support/asset/images/LinkedIn.svg" alt="LinkedIn">
                    </a>
                  </li>
                  <li>
                    <a href="https://www.facebook.com/SAPEducation" target="_blank">
                       <img class="ftnew" src="https://www.sapindiacertification.com/support/asset/images/Facebook.svg" alt="Facebook">
                    </a>
                  </li>
                   <li>
                      <a href="https://twitter.com/sapedu" target="_blank">
                        <img class="ftnew" src="https://www.sapindiacertification.com/support/asset/images/Twitter.svg" alt="Twitter">
                      </a>
                  </li>
                  <li>
                      <a href="https://www.youtube.com/user/SAPEducation2011" target="_blank">
                          <img class="ftnew" src="https://www.sapindiacertification.com/support/asset/images/YouTube.svg" alt="YouTube">
                      </a>
                  </li>
                    </ul>
                </div>
            <div class="menue_footer">
                <div class="mobileLink">
              <a target="_self" data-engagement-tracking-analytics="footer:Privacy" href="http://www.sap.com">
                  <span class="title">SAP</span>
              </a>
                </div>
                <div class="mobileLink">
              <a target="_self" data-engagement-tracking-analytics="footer:Privacy" href="http://www.sap.com/india/about/legal/privacy.html">
                  <span class="title">Privacy</span>
              </a>
                </div>
                <div class="mobileLink">
              <a target="_self" data-engagement-tracking-analytics="footer:Privacy" href="http://www.sap.com/corporate-en/about/legal/terms-of-use.html">
                  <span class="title">Terms of Use</span>
              </a>
                </div>
                <div class="mobileLink">
              <a target="_self" data-engagement-tracking-analytics="footer:Privacy" href="http://www.sap.com/corporate-en/about/legal/impressum.html">
                  <span class="title">Legal Disclosure</span>
              </a>
                </div>
                <div class="mobileLink">
              <a target="_self" data-engagement-tracking-analytics="footer:Privacy" href="http://www.sap.com/corporate-en/about/legal/copyright/index.html">
                  <span class="title">Copyright / Trademark</span>
              </a>
                </div>
                <div class="mobileLink">
                   <a target="_self" data-engagement-tracking-analytics="footer:Privacy" href="https://www.sapindiacertification.com/certification/index.php">
                      <span class="title gcolor">SAP Certification</span>
                  </a>
                </div>
                <div class="mobileLink">
                    <a target="_self" data-engagement-tracking-analytics="footer:Privacy" href="http://www.sap.com/careers/index.html">
                      <span class="title gcolor">SAP Career</span>
                  </a>
                </div>
                <div class="mobileLink">
                    <a target="_self" data-engagement-tracking-analytics="footer:Privacy" href="https://www.sapindiacertification.com/centers/">
                        <span class="title gcolor">SAP Training Centers</span>
                    </a>  
                </div>
                <div class="mobileLink">
                  <a target="_self" data-engagement-tracking-analytics="footer:Privacy" href="https://training.sap.com/shop/training-schedules">
                    <span class="title gcolor">Training Schedules</span>
                </a>  
                </div>
                <div class="mobileLink">
                  <a target="_self" data-engagement-tracking-analytics="footer:Privacy" href="https://www.sapindiacertification.com/certification_code_help.php">
                      <span class="title gcolor">SAP Certification Codes</span>
                  </a>  
                </div>
                <div class="mobileLink">
                    <a>  <span class="title gcolor">@ 2019 SAP SE</span></a>
                </div>
              </div>
          </div>
        </div>
        <!-- End of Mobile Links -->
  </div>