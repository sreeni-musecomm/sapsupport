<nav class="navbar navbar-fixed-top navbar-inverse">
    <div class="container-fluid ">
        <div class="container p-l-0" >
          <div class="col-md-6 col-sm-6 col-xs-6">
            <div class="navbar-header pull-left">
                  <a class="navbar-brand" href="#"><img src="{{ asset('assets/images/sap-logo.png')}}" alt="SAP Training & Certification" class="img-responsive pull-left"></a>
              </div>
          </div>
        </div>
    </div>
</nav>
<div class="container-fluid bg_ban xs-pad">
  <div class="container">
    <div class="row">
          <div class="col-md-9 col-sm-9 col-xs-9">
            <h2 class="ban-head">Support</h2>
          </div>
          <div class="col-md-3 col-sm-3 col-xs-3"></div>
    </div>
  </div>
</div>  

