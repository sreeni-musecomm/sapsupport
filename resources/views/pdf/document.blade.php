<!DOCTYPE html>
<html>
<head>
	<title>PDF</title>
</head>
<body style="font-family: Arial, Helvetica, sans-serif; color: #474747;">
	<div style="width: 960px; margin: auto;">
		<img src="{{ url('assets/images/logo.png')}}">
		<div style="width: 100%; height: 5px; background: #f0ab00; margin-bottom: 20px;">
			<br>
			<h2>Support Ticket No : {{ $view_ticket->ticket_no }}</h2>
			<br>
		<h3 style="border-bottom: 1px solid #c4c4c4; padding-bottom: 5px;">{{$view_ticket->email}} <span style="float: right;">{{ $view_ticket->created_at->format('D-M-Y') }} {{$view_ticket->created_at->format('h:i')}} {{$view_ticket->created_at->format(' A')}}</span></h3>
			
		<p style="color: #666; font-size: 14px; line-height: 22px; margin-bottom: 50px;">{{$view_ticket->message}}</p>

		@if($view_ticket->conversations)
			@foreach($view_ticket->conversations as $value)
			<h3 style="border-bottom: 1px solid #c4c4c4; padding-bottom: 5px;">{{ $value->created_by == 'admin'? 'Support':$view_ticket->email }}<span style="float: right;">{{ $value->created_at->format('D-M-Y') }} {{$value->created_at->format('h:i')}} {{$value->created_at->format(' A')}}</span></h3>
				
			<p style="color: #666; font-size: 14px; line-height: 22px; margin-bottom: 50px;">{!! $value->messages !!}</p>
			@endforeach
		@endif
		
	</div>
</body>
</html>