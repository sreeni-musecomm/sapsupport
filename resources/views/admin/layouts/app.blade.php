<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>
        @section('title')
            SAP ::
        @show
    </title>
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/common.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/header.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/fonts.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/responsive.css') }}" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
         <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
         @stack('css_script')
    </head>
    <body>
        @include('admin.layouts.header')  
        @yield('content')
        <div id="dashboard-footer-div">
        @include('user.layouts.footer') 
        </div>
         
        @yield('chatmodal')
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
        <script src="{{ asset('assets/js/custom.js')}}"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
        @yield('modal')
        <script>
          $(function () {
              $.ajaxSetup({
                  headers: {
                      'X-CSRF-Token': "<?php echo csrf_token() ?>"
                  }
              });
          });
      </script>
      @yield('custom_js')
      @stack('script')
    </body>
</html>