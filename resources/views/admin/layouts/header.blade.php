<!-- <nav class="navbar navbar-fixed-top navbar-inverse">
    <div class="container-fluid" >
          <div class="col-md-6 col-sm-6 col-xs-6">
          <div class="navbar-header pull-left">
                <a class="navbar-brand" href="#"><img src="{{ asset('assets/images/sap-logo.png')}}" alt="SAP Training & Certification" class="img-responsive pull-left"></a>
            </div>
          </div>
    </div>
</nav><br><br><br>


 -->
 <nav class="navbar navbar-fixed-top navbar-inverse">
    <div class="container-fluid" >
      <div class="container" >
        <div class="col-md-6 col-sm-6 col-xs-6">
        <div class="navbar-header pull-left">
              <a class="navbar-brand" href="#"><img src="{{ asset('assets/images/sap-logo.png')}}" alt="SAP Training & Certification" class="img-responsive pull-left"></a>
          </div>
        </div>
        <div class="col-md-6 col-sm-6 col-xs-6 ">
          <!-- <p class="m-t-20 pull-right"><a style="color: #fff;" href="{{ url('logout')}}">Logout</a></p> -->
        </div>
      </div>
    </div>
</nav>

<nav class="navbar navbar-default m-t-50">
  <div class="container-fluid">
    <div class="container" >
      <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span> 
          </button>
      </div>
      <div class="collapse navbar-collapse p-l-0" id="myNavbar">
        <ul class="nav navbar-nav">
          <!-- <li class="{{ Request::is('admin-dashboard') ? 'active' : '' }}">
            <a href="{{ url('admin-dashboard') }}">Home<span class="arrow"></span></a>
          </li> -->

          <li class="dropdown-toggle collapsed {{ Request::is('admin-message-list') ? 'active' : '' }}">
            <a href="{{ url('dashboard')}}">Dashboard<span class="arrow"></span></a>
            <!-- <ul class="dropdown-menu">
              <li class="active"><a href="{{ url('admin-message-list')}}">Message List</a></li>
              <li><a href="new_order.html">Add New Order</a></li>
            </ul> -->
          </li>

        </ul>

        <ul class="nav navbar-nav navbar-right navbar-icons">
          <li class="dropdown-toggle"><a class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user" aria-hidden="true" style="font-size: 20px"></i></a>
              <ul class="dropdown-menu">
                  <!-- <li><a href="#">Profile</a></li> -->
                  <li><a href="{{ url('logout')}}">Logout</a></li>
              </ul>
          </li>
        </ul>
    </div>
    </div>  
  </div>
</nav>

