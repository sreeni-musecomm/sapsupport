<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/chat.css') }}">
<div class="modal fade" id="forward_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 style="padding-left: 15px;" class="modal-title">Forward</h4>
        </div>
        <div class="modal-body">
          <form method="post" action="{{ url('forward-toclient')}}" id="forward-form">
            <div class="row"> 
              <div class="col-md-12">  
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('user_password') ? ' has-error' : '' }} frwd-msg">
                        <!-- <select class="form-control" name="forward_email">
                            <option value="" selected="">Select Email</option>
                            <option value="sreenivasan057@gmail.com">sapindiacertification@sap.com</option>
                        </select> -->
                        <div class="col-md-12 p-l-0">
                          Enter the email
                        </div>
                       <div class="col-md-12 p-l-0">
                          <input type="email" class="form-control" name="forward_email" placeholder="Enter the email">
                        </div>
                    </div>
                    <input type="hidden" name="user_id" class="users_id">
                    <input type="hidden" name="message_id" class="messages__id">
                    <span class="error hidden sub_for">
                      <strong class="error_msg forward_text"></strong>
                    </span>
                  </div>
                  <div class="col-md-6">
                      
                 <div class="col-md-12 form-group frwd-msg p-l-0">  
                    <div class="form-group{{ $errors->has('user_password') ? ' has-error' : '' }}  frwd-msg">
                     <div class="col-md-12 p-l-0">
                       CC for other admin
                     </div>
                   </div>
                </div>
                <div class="col-md-12 p-l-0">  
                  <div class="form-group{{ $errors->has('user_password') ? ' has-error' : '' }} frwd-msg">
                      <input name="cc_admin" placeholder="cc for other admins*" class="form-control">
                  </div>
                  <span class="error hidden sub_cc_admin">
                    <strong class="error_msg ccadmin_text"></strong>
                  </span>
                </div>
                  </div>
              </div>
              <div style="line-height:40px;" class="col-md-12 p-t-10">  
                <div class="form-group{{ $errors->has('user_password') ? ' has-error' : '' }} frwd-msg">
                 <div class="col-md-6">
                   Do you want cc to user ?
                 </div>
                 <div class="col-md-3">
                  Yes <input class="filter-radio" type="radio" value="1" checked="" name="cc_need">
                 </div>

                 <div class="col-md-3">
                   No <input class="filter-radio" type="radio" value="0" name="cc_need">
                 </div>
               </div>
                <input type="hidden" name="user_id" class="users_id">
                <input type="hidden" name="message_id" class="messages__id">
              </div>
               <div style="line-height:40px;" class="col-md-12 form-group frwd-msg">  
                  <div class="form-group{{ $errors->has('user_password') ? ' has-error' : '' }} frwd-msg">
                   <div class="col-md-6">
                     Do you want add extra text to forward ?
                   </div>
                   <div class="col-md-3">
                    Yes <input type="radio" value="1" name="text_need" class="text_need filter-radio">
                   </div>

                   <div class="col-md-3">
                     No <input type="radio" value="0" checked="true" name="text_need" class="text_need filter-radio">
                   </div>
                 </div>
                  <input type="hidden" name="user_id" class="users_id">
                  <input type="hidden" name="message_id" class="messages__id">
              </div>
              <div class="col-md-12 hidden extra_text">  
                <div style="margin-top:15px; padding:0px 15px;" class="form-group{{ $errors->has('user_password') ? ' has-error' : '' }} frwd-msg">
                    <textarea name="extra_text" id="_message" class="form-control"></textarea>
                </div>
                <span class="error hidden sub_cc_text">
                  <strong class="error_msg cc_text"></strong>
                </span>
              </div>
              
              </div>
          </form>
        </div>
        <div class="modal-footer">
          <button class="btn btn-warning forward-submit" type="button">Forward</button>
          <button style="position:relative; top:3px;" type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
        </div>
        
      </div>
      
    </div>
  </div>
