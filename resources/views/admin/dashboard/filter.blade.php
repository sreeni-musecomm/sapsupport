<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/chat.css') }}">
<div class="modal fade" id="filter_modal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"> Filter</h4>
        </div>
          <form method="get" action="">
        <div class="modal-body">
            <div class="row"> 
              <div class="col-md-6">  
                <label>Enter the ticket id</label>
                <div class="form-group{{ $errors->has('ticket_id') ? ' has-error' : '' }}">
                    <input type="text" class="form-control" name="ticket_id" placeholder="Please Enter the ticket id">
                </div>
                <input type="hidden" name="user_id" class="users_id">
                <input type="hidden" name="message_id" class="messages__id">
                <span class="error hidden sub_for">
                  <strong class="error_msg forward_text"></strong>
                </span>
              </div>
              <div class="col-md-6">  
                <label>Enter the email ID</label>
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                   <div class="col-md-12 p-l-0">
                     <input type="text" class="form-control" placeholder="Please enter the email" name="email">
                   </div>
                </div>
                <input type="hidden" name="user_id" class="users_id">
                <input type="hidden" name="message_id" class="messages__id">
              </div>
              </div>
        </div>
        <div class="modal-footer">
          <button class="btn btn-warning" type="submit">Search</button>
          <button type="button" class="btn btn-warning" data-dismiss="modal">Close</button>
        </div>
          </form>
        
      </div>
      
    </div>
  </div>
