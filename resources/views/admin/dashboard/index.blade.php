@extends('admin.layouts.app')
@section('title')
@parent 
Dashboard
@stop
@section('content')
<style type="text/css">
        .read {
            color: red
        }
        .unread{
            color: green   
        }

        .navbar .container, #message-heading-container .container, #message-table-container .container, #dashboard-footer-div .container
        {
            width:100%;
            max-width:100% !important;
        }

    </style>
    <div id="message-heading-container" class="container-fluid">
        <div class="container" style="padding-right:30px; padding-left:30px;">
            <div class="row">
                <div class="col-md-12 p-l-r-0 content-header-2">
                @if(Session::has('message'))
               <div class="alert alert-success fade in alert-dismissible" style="margin-top:18px;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                     {{  Session::get('message') }}
                </div>
                @endif  
                
                 @if(Session::has('message_error'))
               <div class="alert alert-danger fade in alert-dismissible" style="margin-top:18px;">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
                    {{  Session::get('message_error') }}
                </div>
                @endif 
                    <div class="col-md-4 col-sm-3">
                        <h3>Message List({{ $message_count }})</h3>
                    </div>
                    <!-- <div class="col-md-3 pull-right report_option_box">
                        <div class="form-group">

                            <a class="btn  btn-primary login_submit" data-toggle="modal" data-target="#myModal">Compose</a>
                            <a class="btn btn-warning login_submit" href="{{url('compose_message_admin')}}" >Compose</a>
                        </div>  
                    </div> -->
                    <div class="col-md-8 col-sm-9 pull-right report_option_box">
                        <div class="form-group">
                            <button class="btn-warning" data-target="#filter_modal" data-toggle="modal" >Filter</button>
                            <a href="{{ url('dashboard')}}"><button class="btn-warning">Reset</button></a>
                            <a href="{{ url('dashboard?unread=1')}}"><button class="btn-warning" >Unread Message</button></a>
                            <!-- <a class="btn  login_submit" href="{{url('compose_message_admin')}}" >Compose</a> -->
                            {{ $message_data->appends(request()->input())->links() }}
                        </div>  
                    </div>
                    <div class="clearfix"></div>
                </div>
               
            </div>

            </div>
            
        </div> 
    <div class="container-fluid" id="message-table-container">
        <div class="container" style="padding-right:30px; padding-left:30px;">
        <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table id="dashboard-msgs-table" class="table table-bordered" border="1" cellspacing="0" cellpadding="2" width="100%" style="font-size: 12px; margin-top:10px;">
                            <thead style="background: #f3f3f3; color: #545454;">
                                <tr>
                                    <th>S.no</th>
                                    <th>Ticket No</th>
                                    <th>Email</th>
                                    <th>Subject</th>
                                    <th>Status</th>
                                    <th>Created at</th>
                                    <th>Last reply by</th>
                                    <th>Reply to</th>
                                    <th>Forward</th>
                                    <th>Generate Pdf</th>
                                </tr>
                            </thead>
                     
                            <tbody class="tbl_content">
                                @foreach($message_data as $key=>$message_val)
                                <tr class="" title="">
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $message_val->ticket_no }}</td>
                                    <td>{{ $message_val->email }}</td>
                                    <td>{{ $message_val->typeofinfo }}</td>
                                    <td class="{{ $message_val->status == 0 ? 'read':'unread' }}">{{ $message_val->status == 0 ? 'unread':'read' }}</td>
                                    <td >{{ $message_val->created_at->format('d-m-Y h:i') }}</td>
                                    @if($message_val->reply_by)
                                    <td>{{ $message_val->reply_by }}</td>
                                    @else
                                    <td>{{ "no reply" }}</td>
                                    @endif
                                    <td ><a href="{{ url('ticket-view/').'/'.$message_val->ticket_url}}" data-toggle="modal" class="edit-modal-admin btn-warning" data-user="{{ $message_val->status }}" data-id="{{ $message_val->id }}" data-update="" data-url="{{url('get-messages-admin')}}">{{ $message_val->reply_status == 1 ? 'Replied':'Reply'}}</a></td>
                                    <td><a data-target="#forward_modal" data-toggle="modal" class="forward_by btn-warning" data-user="{{ $message_val->user_id }}" data-id="{{ $message_val->id }}" data-update="" data-url="{{url('get-messages-admin')}}">{{ $message_val->forward_status == 1 ? 'Forwarded':'Forward'}}</a></td>
                                    <td><a target="_blank" class="forward_by btn-warning" href="{{ url('pdf-gen').'/'.$message_val->ticket_url}}">Generate Pdf</a></td>

                                </tr>
                                @endforeach
                                @if($message_count == 0)
                                <tr>
                                        <td colspan="10" align="center">No record's</td>
                                @endif
                                </tr>
                            </tbody>
                        </table>
                  
                    </div>
                </div>
            
        </div>
        </div>
    </div>
@include('admin.dashboard.forward')
@include('admin.dashboard.filter')
@stop
@section('custom_js')
<script type="text/javascript" src="{{ asset('assets/js/custom-style.js')}}"></script>

<!-- Content ends -->

<script src="https://cdn.ckeditor.com/4.5.9/standard/ckeditor.js"></script>
<script type="text/javascript">
CKEDITOR.replace( 'extra_message' );
</script>

@stop

@push('css_script')
<link href="{{ asset('assets/css/admin_style.css') }}" rel="stylesheet">
@endpush
