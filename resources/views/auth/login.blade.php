<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="robots" content="NOINDEX, NOFOLLOW"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="keyword" content="Retina">

    <title>SAP | Support Login</title>
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/style_login.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/header.css') }}" rel="stylesheet"> 
    <link href="{{ asset('assets/css/style-responsive.css') }}" rel="stylesheet">    
    <style>
        
    </style>
  </head>

  <body>
        <div id="" class="container-fluid navbar-fixed-top p-t-20" style="background:#000000;height:75px;">
                <div class="container">
                   <div class="row ">
                        {{-- <a class="col-md-5 col-sm-6 col-xs-6 logo" style="padding-top: 20px;">
                            <img src="{{asset('assets/images/sap-logo.png')}}" alt=""  class="img-responsive">
                            <strong style="padding-left: 22px;color:#ffffff;font-size: 18px"> | Support Admin</strong>
                        </a> --}}
                        <a href="javascript://" class="logo">
                            <img src="{{asset('assets/images/sap-logo.png')}}" alt="">
                            <strong style="padding-left: 22px;">  Support Admin </strong>
                        </a>
                    </div>
                </div>
            </div>

      <!--       MAIN CONTENT      -->

      <div id="login-page">
        <div class="container">
          <form class="form-login" name="loginForm" method="post" action="{{ url('/login') }}" >
               {{ csrf_field() }}
                <h2 class="form-login-heading" style="text-transform:none !important"> Log In</h2>
                <div class="login-wrap">
                 @if (session('status'))
                    <div class="alert alert-danger">
                        {{ session('status') }}
                    </div>
                @endif
                <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="text"  class="form-control" placeholder="Email ID" name="email"  value="{{ old('email') }}">
                       
                         @if ($errors->has('email'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('email') }}</strong>
                              </span>
                          @endif
                    </div>
                    <div class="form-group has-feedback {{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" class="form-control" placeholder="Password" name="password"  value="{{ old('password') }}">
                      
                         @if ($errors->has('password'))
                              <span class="help-block">
                                  <strong>{{ $errors->first('password') }}</strong>
                              </span>
                          @endif
                    </div>
                    <button id="login_btn" class="btn btn-theme btn-block" type="submit" onclick="validate_data()"><i class="fa fa-lock"></i> LOG IN</button>
                    
                    
                     <a href="{{url('aspirant/auth/register')}}" id="reg_btn" class="hidden btn btn-theme btn-block" type="submit" onclick=""><i class="fa fa-lock"></i> Register </a>
        
                </div>
                
              </form>

                  <!-- Modal -->
                  <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade">
                      <div class="modal-dialog">
                          <div class="modal-content">
                              <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                  
                              </div>
                              <div class="modal-body">
                                  <p>Enter your e-mail address below to reset your password.</p>
                                  <input type="text" name="email" placeholder="Email" autocomplete="off" class="form-control placeholder-no-fix">
        
                              </div>
                              <div class="modal-footer">
                                  <button data-dismiss="modal" class="btn btn-default" type="button">Cancel</button>
                                  <button class="btn btn-theme" type="button">Submit</button>
                              </div>
                          </div>
                      </div>
                  </div>
                  <!-- modal -->
                
        
        </div>
      </div>
      @include('user.layouts.footer') 
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
      <script src="{{ asset('assets/js/bootstrap.min.js')}}"></script>
      <script src="{{ asset('assets/js/jquery.backstretch.min.js')}}"></script>
      <script>
           $.backstretch("{{url('assets/images/login-bg.jpg')}}", {speed: 500});
      </script>
 
<script>

    var window_height = $(window).height();
    var document_height = $(document).height();

    //alert('Window : '+window_height+', Document : '+document_height);

    if(window_height>=document_height)
    {
        $('#login-page').css("min-height", (window_height)-75+"px");
    }

    var login_height = $('.form-login').height();
    var login_margin_top = Math.ceil((window_height-login_height)/2);

    //alert(login_margin_top);

    $('.form-login').css('margin-top', login_margin_top+'px')


function validate_data(){
    login=document.loginForm.usrname.value;
    pwd=document.loginForm.pwd.value;

    if (trim(login)=="")
    {
        $('#err').html("Please enter User ID:")
        document.loginForm.usrname.focus();
        return false;
    }
    if (trim(pwd)=="")
    {
        $('#err').html("Please enter password");
        document.loginForm.pwd.focus();
        return false;
    }
    document.loginForm.submit();

}

function clear_data(){
    document.loginForm.usrname.value="";
    document.loginForm.pwd.value="";
}
$('#pass').keypress(function(e)
{
        if(e.which == 13)
        {//Enter key pressed            
            $('#login_btn').click()
        }
    });
    </script>

  </body>
</html>
