<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NewEnquiry extends Model
{
    protected $table = "new_enquiries";

    public static function ticketNo()
    {
    	$query 	= NewEnquiry::select('ticket_no')
								->orderBy('id','DESC')
								->limit(1)
								->where('ticket_no','!=',"")
								->get();
		if(isset($query[0])){
			$number 			= preg_split('/(,?\s+)|((?<=[a-z])(?=\d))|((?<=\d)(?=[a-z]))/i', $query[0]->ticket_no);
			$ticket_no 		= "SAP000".($number[1]+1);
		}
		else{
			$ticket_no = "SAP0001";
		}
		return $ticket_no;
    }

    public function conversations()
    {
	    return $this->hasMany('App\Models\Conversations','messages_id','id');
    }
}
