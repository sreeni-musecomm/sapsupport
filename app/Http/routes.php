<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::auth();
Route::resource('/','UserController');

Route::get('/home', 'HomeController@index');

/*New enquiry post*/
Route::post('new-enquiry','UserController@postNewEnquiry');

/*thank you Page*/
Route::any('thank-you','UserController@getThankyou');

/*ex enquiry */
Route::post('ex-enquiry','UserController@postExEnquiry');

/*ticket view*/
Route::get('ticket-view/{id}','UserController@ticketView');

/*reply-user*/
Route::post('reply-user','UserController@postReplyUser');

Route::get('logout',function(){
            \Auth::guard()->logout();
            \Auth::logout();
            return redirect('login');
        });
Route::post('forward-toclient','AdminController@postForwardToclient');
/*admin*/

Route::resource('dashboard','AdminController');


/*pdf Genrater*/
Route::get('pdf-gen/{id}','PdfController@getGeneratePdf');