<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use PDF;
use App\Http\Requests;
use App\Models\NewEnquiry;

class PdfController extends Controller
{
    public function getGeneratePdf($id)
    {
    	$ticket_details = NewEnquiry::where('ticket_url',$id)->with('conversations')->first();
    	$data = ['view_ticket'=>$ticket_details];
    	return PDF::html('pdf.document', $data, 'Tickets');
    }
}
