<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use Config;
use App\Http\Requests;
use App\Models\NewEnquiry;
use App\Jobs\AdminReply;
use App\Models\Conversations; 
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index(Request $request)
    {
    	return view('user.user.index',['typeOfInfo' => $this->typeOfInfo()]);
    }
    public function typeOfInfo()
    {
    	return ["Cloud Certification Enquiry","Direct Certification Enquiry","SAP Employee Enquiry","Specialist Enquiry","SAP Learning Hub Enquiry","SAP TechED 2018 Enquiry","Capgemini Q32018 Enquiry","General Enquiry","Others"
    	];
    }

    public function postNewEnquiry(Request $request)
    {
         if($request->hasFile('file_path')){
        	$file_type = array("pdf", "xlsx", "doc", "xls","docx");
            if(!in_array($request->file('file_path')->getClientOriginalExtension(), $file_type)){
                \Session::flash('message', 'The file must be a file of type: pdf, xlsx, doc, xls, docx.');
                return redirect()->back()->withInput();
            }
        }
    	$validation          = $this->validation($request);
    	if($validation->fails()){
    		return redirect()->back()->withInput()->withErrors($validation->messages());
    	}
       
		try {
			$this->storeNewEnquiry($request);
		} catch (\Exception $e) {
			return "InterNal server Error: Database can't insert";
		}
		return redirect('thank-you');
    	
    }
    public function storeNewEnquiry($request)
    {
    	$new_enquiry 				= new NewEnquiry;
    	$new_enquiry->user_type 	= $request->get('user_type');
        $new_enquiry->email         = $request->get('email');
        if($new_enquiry->user_type == 1){
             $get_old_details = NewEnquiry::where('email',$new_enquiry->email)->first();
             $new_enquiry->f_name       = $get_old_details->f_name;
            $new_enquiry->l_name        = $get_old_details->l_name;
        } else{
            $new_enquiry->f_name        = $request->get('first_name');
            $new_enquiry->l_name        = $request->get('last_name');
        }
    	$new_enquiry->typeofinfo 	= $request->get('typeofinfo');
    	$new_enquiry->certi_others 	= $request->get('certi_others',NULL);
    	if($request->hasFile('file_path')){
    		$file_name 	= rand(9999,100000).'.'.$request->file('file_path')->getClientOriginalExtension();
    		$request->file('file_path')->move(public_path(Config::get('constant.file_path').NewEnquiry::ticketNo()."/"), $file_name);
    		$new_enquiry->file_path = NewEnquiry::ticketNo().'/'.$file_name;;
    	}
    	$new_enquiry->message 		= $request->get('message',NULL);
    	$new_enquiry->ticket_no 	= NewEnquiry::ticketNo();
    	$new_enquiry->ticket_url 	= md5(NewEnquiry::ticketNo());
    	$new_enquiry->ip_address 	= $request->ip();
    	$new_enquiry->save();
    	$new_enquiry->save();
    }
    public function getThankyou()
    {
    	return view('user.user.thankyou');
    }

    public function validation($request)
	{
		$rules  =[
				    'user_type' 			=> 'required',
				    'typeofinfo' 			=> 'required',
				    'message' 				=> 'required|max:500',
				    'file_path' 			=> 'max:2048'
				];
        if($request->get('user_type') == 0){
            $rules['first_name']    = 'required|regex:/^[a-zA-Z]+$/u|max:255|min:2';
            $rules['last_name']     = 'required|regex:/^[a-zA-Z]+$/u|max:255|max:100';
            $rules['email']         = 'email|required|max:500|unique:new_enquiries,email';
        }  
        if($request->get('user_type') == 1){
           $rules['email']         = 'email|required|max:500|exists:new_enquiries,email';
        }
		if($request->get('typeofinfo') == 'Others'){
			$rules['certi_others']  = 'required';
		}
		 $messages = [
            'typeofinfo.required' => 'The type of certification field is required.',
            'certi_others.required' => 'The others field is required.',
        ]; 
		return Validator::make($request->all(), $rules, $messages);
	} 

	public function postExEnquiry(Request $request)
	{
		$validation          = $this->validationex($request);
    	if($validation->fails()){
    		return response(['status'=>false,'result'=>$validation->messages(),"toke"=>csrf_token()], 200);
    	}
    	return response(['status'=>true,'result'=>NewEnquiry::where('email',$request->get('email'))->get(),"toke"=>csrf_token()],200);

	}

	public function validationex($request)
	{
        $rules = ['email'=> 'email|required||max:500|exists:new_enquiries,email'];
        $messages = [
            'email.email' => 'Please enter a valid email ID.',
            'email.required' => 'Please enter a valid email ID.',
        ]; 
		return Validator::make($request->all(),$rules,$messages);
	}

	/*ticketView*/
	public function ticketView($id)
	{
		$ticket_details = NewEnquiry::where('ticket_url',$id)->with('conversations')->first();
        if($ticket_details)return view('user.user.ticketview',['ticket_details'=>$ticket_details]);
        else return view('errors.404');

	}

	public function postReplyUser(Request $request)
	{
		$validation          = $this->replyValidation($request);
    	if($validation->fails()){
    		return redirect()->back()->withInput()->withErrors($validation->messages());
    	}
		$ticket_details = NewEnquiry::where('ticket_url',$request->get('redirect_id'))->with('conversations')->first();
		$conversations 				= new Conversations;
		$conversations->messages_id = $ticket_details->id;
		$conversations->messages 	= $request->get('message',NULL);
		if($request->hasFile('file_path')){
		    
		    $file_type = array("pdf", "xlsx", "doc", "xls","docx");
            if(!in_array($request->file('file_path')->getClientOriginalExtension(), $file_type)){
                \Session::flash('message', 'The file must be a file of type: pdf, xlsx, doc, xls, docx.');
                return redirect()->back()->withInput();
            }
		    
    		$file_name 	= rand(9999,100000).'.'.$request->file('file_path')->getClientOriginalExtension();
    		$request->file('file_path')->move(public_path(Config::get('constant.file_path').$ticket_details->ticket_no."/"), $file_name);
    		$conversations->file_path 	=  $ticket_details->ticket_no.'/'.$file_name;;
    	}
    	$conversations->status = true;
        if(Auth::user()){
           $conversations->created_by = 'admin';
           $ticket_details->status = true;
           $ticket_details->reply_status = true;
           $ticket_details->save();
        } else{
           $conversations->created_by = 'user';
           $ticket_details->reply_status = false;
           $ticket_details->status = false;
           $ticket_details->save();
        }

        if($conversations->save()){
            if(Auth::user()){
                try {
                    $ticket_details->support_msg = $request->get('message',NULL);
                    return self::mailSendAdminreply($ticket_details);
                } catch (\Exception $e) {
                    return redirect('ticket-view'.'/'.$ticket_details->ticket_url);        
                }
            }
            return redirect('thank-you');
        }

	}
    public function mailSendAdminreply($ticket_details)
    {
        $message_con = Conversations::where('messages_id',$ticket_details->id)->where('created_by','user')->first();
        if($message_con){
            $ticket_details->last_message = $message_con->messages;
        } else{
            $ticket_details->last_message = $ticket_details->message;
        }
        $this->dispatch(new AdminReply($ticket_details));
        return redirect('dashboard'); 
    }
	public function replyValidation($request)
	{
		return Validator::make($request->all(), [
					'redirect_id'			=> 'required',
				    'message' 				=> 'required',
				    'file_path' 			=> 'max:2048'
				]);
	}
}
