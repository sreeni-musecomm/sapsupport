<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use App\Http\Requests;
use App\Models\Forwards;
use App\Models\NewEnquiry;
use App\Jobs\ForwordClient;
use App\Models\Conversations;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {
        $message_data = self::searchFilter($request);
    	foreach ($message_data as $key => $value) {
            $value->message = $value->message;
            $conversation  = Conversations::where('messages_id',$value->id)->orderBy('created_at', 'desc')->first();
            if($conversation){
                $message_data[$key]->lastMessage = $conversation->messages;
                $message_data[$key]->lastMessage_id = $conversation->id;
                $message_data[$key]->reply_by = $conversation->created_by;
            } else{
                $message_data[$key]->lastMessage = null;
                $message_data[$key]->reply_by = null;
                $message_data[$key]->lastMessage_id = $value->id;
            }
        }
        return view('admin.dashboard.index',[
    				'message_count' => $message_data->total(),
    				'message_data'	=> $message_data,
    			]);
    }

    public function searchFilter($request)
    {
         $filter = NewEnquiry::orderBy('created_at', 'desc');
         if($request->get('ticket_id')) $filter->whereIn('ticket_no',explode(",", $request->get('ticket_id')));
         if($request->get('email')) $filter->whereIn('email',explode(",", $request->get('email')));
         if($request->get('unread'))$filter->where('status',false);
         return $filter->paginate(100);
    }

    public function postForwardToclient(Request $request)
    {
    	$forwardValidation = self::forwardValidation($request);
        if($forwardValidation->fails()) return response()->json(['status'   => 'error','messages' => $forwardValidation->errors()],203);
        $message = NewEnquiry::where('id',$request->input('message_id'))->with('conversations')->first();
        $message->forward_status = true;
        $message->save();
        $message->toaddress = $request->input('forward_email');
        $message->name = \Auth::user()->name;
        $forwards = new Forwards;
        $forwards->messages_id = $request->input('message_id');
        $forwards->user_id = \Auth::user()->id;
        $forwards->forward_to = $request->input('forward_email');
        $cc_mails = [];
        if($request->input('cc_need') == 1){
        	$cc_mails[] = $message->email;
            $forwards->cc_user = $message->email;
        } 
        if($request->input('cc_admin')){
            $cc_mails[] = $request->input('cc_admin');
            $forwards->cc_admin = $request->input('cc_admin');
        } 
        if($request->input('text_need') == 1){
            $forwards->text_need = $request->input('text_need');
            $message->extra_text = $request->input('extra_text');
            $forwards->extra_text = $request->input('extra_text');
        } else{
            $message->extra_text = null;
            $forwards->text_need = null;
        }
        
        $message->ccaddress = $cc_mails;
        $message_con = Conversations::where('messages_id',$message->id)->where('created_by','user')->first();
        if($message_con){
            $message->last_message = $message_con->messages;
        } else{
            $message->last_message = $message->message;
        }
        try {
            $this->dispatch(new ForwordClient($message));
            $forwards->save();
        } catch (\Exception $e) {
            \Session::flash('message_error', trans('custom_messages.message_forward_failer'));
            $message = NewEnquiry::where('id',$request->input('message_id'))->with('conversations')->first();
            $message->forward_status = false;
            $message->save();
            return response()->json(['status'   => 'fail',
                'messages' => $request->all(),
                'redirectUrl' => url('dashboard'),
            ],200);     
        }
       \Session::flash('message', trans('custom_messages.message_forward'));
        return response()->json(['status'   => 'success',
            'messages' => $request->all(),
            'redirectUrl' => url('dashboard'),
        ],200);
    }
    protected function forwardValidation($request)
    {
       $rules = [
                    'forward_email'   => 'email|required|max:500',
                    'cc_admin'        => 'email|max:50',
            ];
        if($request->input('text_need') == 1){
             $rules['extra_text'] = 'required';
        }
        return \Validator::make($request->all(), $rules);
    }

}
