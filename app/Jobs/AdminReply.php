<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\NewEnquiry;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminReply extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    
    protected $user;
    protected $lastmessage;
    protected $support_msg;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(NewEnquiry $user)
    {
        $this->user = $user;
        $this->lastmessage = $user->last_message;
        $this->toaddress = $user->toaddress;
        $this->support_msg = $user->support_msg;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $mailer->send('emails.admin_reply', ['ticket_details' => $this->user,'lastmessage' => $this->lastmessage,'support_msg'=>$this->support_msg], function ($m) {
            $m->from('no-reply@sapindiacertification.com', 'SAP India Certification Support');
            $m->to($this->user->email)->subject($this->user->typeofinfo." | SAP India Certification | Ticket no : ".$this->user->ticket_no);
        });
    }
}
