<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Models\NewEnquiry;
use Illuminate\Contracts\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ForwordClient extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;
    protected $user;
    protected $name;
    protected $cc_mail;
    protected $toaddress;
    protected $extra_text;
    protected $last_message;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(NewEnquiry $user)
    {
        $this->user = $user;
        $this->cc_mail = $user->ccaddress;
        $this->toaddress = $user->toaddress;
        $this->name = $user->name;
        $this->extra_text = $user->extra_text;
        $this->last_message = $user->last_message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mailer $mailer)
    {
        $mailer->send('emails.forward', ['ticket_details' => $this->user,'extra_text'=>$this->extra_text,'last_message'=>$this->last_message], function ($m) {
            $m->from('no-reply@sapindiacertification.com', 'SAP India Certification Support');
            if($this->cc_mail) {
                $m->cc($this->cc_mail);
            }
            $m->to($this->toaddress)->subject($this->user->typeofinfo." | SAP India Certification | Ticket no : ".$this->user->ticket_no);
        });
    }
}
