$(document).ready(function(){
	$('#typeofinfo').val() == 'Others' ? $('#others').css('display','block'):$('#others').css('display','none');
})
$(document).on('change','#typeofinfo',function(){
$(this).val() == 'Others' ? $('#others').css('display','block'):$('#others').css('display','none');
});
var new_enq = $('#new-enquiry'),ex_enq=$('#exis-enquiry');
$(document).on('click','.new-enquiry',function(){
	new_enq.removeClass('hidden');
	ex_enq.addClass('hidden');
});
$(document).on('click','.exsisting-enquiry',function(){
	new_enq.addClass('hidden');
	ex_enq.removeClass('hidden');
});

ex_enq.submit(function(e){
	e.preventDefault();
	var x = ex_enq.serializeArray();
    var datas = new FormData();
    $.each(x, function(i, field){
    	datas.append(field.name,field.value);
    });
    $.ajax({
			url : ex_enq.attr("action"),
			type : 'post',
			data : datas,
			headers:{'X-CSRF-TOKEN': $('input[name=_token]').val()},
			contentType: false,
   			processData: false,
			beforeSend : function(){
				$('.preloader').show();
			},
			complete: function(){
				$('.preloader').hide();
			},
			success : function(response){
				if(response.status){
					var redirect_url = $('#ticket-details').data('url');
					$('.ticket-check').prop('disabled',true);
					$('#ticket-details').html(" ");
					for(var i = 0; i<response.result.length;i++){
					$('#ticket-details').append('<div class="row m-t-b-20"><div class="col-md-12"><div class="col-md-1"><span>'+(i+1)+'</span></div><div class="col-md-5"><label>Support ticket no.'+response.result[i].ticket_no+'</label></div><div class="col-md-5"><label>Date:</label><span>'+response.result[i].created_at+'</span></div><div class="col-md-1"><a href="'+redirect_url+"/"+response.result[i].ticket_url+'">view</a></div></div></div>');
					}
				} else{
					$('.email-error strong').html(response.result.email[0]);
				}
			}
		});
});