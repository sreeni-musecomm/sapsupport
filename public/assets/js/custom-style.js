function send_message(value){
	var data = $('#'+value).serializeArray();
	var url = $('#'+value).attr('action');
	data.push({ name: "message", value: CKEDITOR.instances.message.getData() });
	message_sent(url,data,function(res){
		if(res.status == "error"){
			if(res.messages.subject){
				$('.sub_msg').removeClass('hidden');
				$('.subject').text(res.messages.subject[0]);
			} else{
				$('.sub_msg').addClass('hidden');
			}
			if(res.messages.message){
				$('.msg_msg').removeClass('hidden');
				$('.message').text(res.messages.message[0]);
			} else{
				$('.msg_msg').addClass('hidden');
			}
			if(res.messages.user_email){
				$('.email_msg').removeClass('hidden');
				$('.error_email').text(res.messages.user_email[0]);
			} else{
				$('.email_msg').addClass('hidden');
			}	
		} else{
			$('.sub_msg').addClass('hidden');
			$('.msg_msg').addClass('hidden');
			$('.email_msg').addClass('hidden');
		}
		if (res.redirectUrl) {
            document.location.href = res.redirectUrl;
        }
	});	
};
function send_message_image(value){
	var data = new FormData($('#'+value)[0]);
	var url = $('#'+value).attr('action');
	data.append('message', CKEDITOR.instances.message.getData());
	replyMessage(url,'POST',data,function(res){
		if(res.status == "error"){
			if(res.messages.subject){
				$('.sub_msg').removeClass('hidden');
				$('.subject').text(res.messages.subject[0]);
			} else{
				$('.sub_msg').addClass('hidden');
			}
			if(res.messages.message){
				$('.msg_msg').removeClass('hidden');
				$('.message').text(res.messages.message[0]);
			} else{
				$('.msg_msg').addClass('hidden');
			}
			if(res.messages.user_email){
				$('.email_msg').removeClass('hidden');
				$('.error_email').text(res.messages.user_email[0]);
			} else{
				$('.email_msg').addClass('hidden');
			}	
		} else{
			$('.sub_msg').addClass('hidden');
			$('.msg_msg').addClass('hidden');
			$('.email_msg').addClass('hidden');
		}
		if (res.redirectUrl) {
            document.location.href = res.redirectUrl;
        }
	});
}

$(document).on('click','.forward_by',function(){
	$('.users_id').val($(this).attr('data-user'));
	$('.messages__id').val($(this).attr('data-id'));
});


$(document).on('click','.forward-submit',function(){
	var data = $('#forward-form').serialize();
	var url = $('#forward-form').attr('action');
	message_sent(url,data,function(res){
		if(res.status == "error"){
			if(res.messages.forward_email){
				$('.sub_for').removeClass('hidden');
				$('.forward_text').text(res.messages.forward_email[0]);
			} else{
				$('.sub_for').addClass('hidden');
			}
		} else{
			$('.sub_for').addClass('hidden');
		}
		if (res.redirectUrl) {
            document.location.href = res.redirectUrl;
        }
	});
});

$(document).on('click','.new-user-submit',function(){
	var data = $('#register_form').serialize();
	var url = $('#register_form').attr('action');
	message_sent(url,data,function(res){
		if(res.status == "error"){
			if(res.messages.name){
				$('.sub_name').removeClass('hidden');
				$('.forward_name').text(res.messages.name[0]);
			} else{
				$('.sub_name').addClass('hidden');
			}
			if(res.messages.email){
				$('.sub_email').removeClass('hidden');
				$('.forward_email').text(res.messages.email[0]);
			} else{
				$('.sub_email').addClass('hidden');
			}
			if(res.messages.password){
				$('.sub_password').removeClass('hidden');
				$('.forward_password').text(res.messages.password[0]);
			} else{
				$('.sub_password').addClass('hidden');
			}
			if(res.messages.password_confirmation){
				$('.sub_confirm').removeClass('hidden');
				$('.forward_confirm').text(res.messages.password_confirmation[0]);
			} else{
				$('.sub_confirm').addClass('hidden');
			}	
		} else{
			$('.sub_name').addClass('hidden');
			$('.sub_email').addClass('hidden');
			$('.sub_password').addClass('hidden');
			$('.sub_confirm').addClass('hidden');
		}
		if (res.redirectUrl) {
            document.location.href = res.redirectUrl;
        }
	});	
});

function replyMessage(post_url,request_method,form_data,cb) {
    $.ajax({
        url : post_url,
        cache: false,
        contentType: false,
        processData: false,
        type: request_method,
        data : form_data,
        enctype: 'multipart/form-data',
    }).done(function(response){ //
         cb(response);
    });
}

function message_sent(url,data,cb){
	var results = [];
	$.ajax({
	    type : 'POST',
	    url : url,
	    data : data,
	    dataType: "json",
	    success: function(result){
	    	cb(result);
	    }
	});
}

$(document).on('click','.text_need',function(){
	if($(this).val() == 1){
		$('.extra_text').removeClass('hidden');
	} else{
		$('.extra_text').addClass('hidden');
	}
})
$(document).on('click','.read_more_mesage',function(){
	var url 		= $(this).attr('data-url');
	var last_msg  	= $(this).attr('data-last');
	var id  		= $(this).attr('data-id');
	var data 		= [];
	data.push({ name: "last_msg", value: last_msg  });
	data.push({ name: "id", value: id  });
	message_sent(url,data,function(res){
		if(res.status == "success"){
			$('.readmore_users').html(res.messages);
			$('.reply_redirect').attr('href',res.redirectUrl);
		}
	});
});