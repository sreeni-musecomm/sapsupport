function ValidateFileUpload(file_id) {
    var fuData = document.getElementById(file_id);
    var FileUploadPath = fuData.value;
    if (FileUploadPath == '') {
        alert("Please upload an image");
    } else {
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
        if (Extension == "doc" || Extension == "docx" || Extension == "pdf" ||
            Extension == "xlsx" || Extension == "xls") {
            if (fuData.files && fuData.files[0]) {
                var size = fuData.files[0].size;
                console.log(size);
                if (1978699 < size) {
                    $('#_file > label').html("Your file size is more than 2MB");
                    $('#_file').removeClass('hidden');
                    $('#'+file_id).val('');
                    return false;
                } else{
                    $('#_file').addClass('hidden');
                } 
            }
        } else {
            $('#_file > label').html("Invalid Extension!");
            $('#_file').removeClass('hidden');
            $('#'+file_id).val('');
        }
    }
}