$(document).ready(function(){
	$('#typeofinfo').val() == 'Others' ? $('#others').css('display','block'):$('#others').css('display','none');
	var $radios = $('input:radio[name=user_type]');
	if($radios[0].checked == true){
		$('.form_name').removeClass('hidden');
	} else{
		$('.form_name').addClass('hidden');
	}
});
function checkUser(data){
    if(data == "user_new"){
        $('.form_name').removeClass('hidden');
    } else{
        $('.form_name').addClass('hidden');
        
    } 
}
$(document).on('change','#typeofinfo',function(){
$(this).val() == 'Others' ? $('#others').css('display','block'):$('#others').css('display','none');
});


var new_enq = $('#new-enquiry'),ex_enq=$('#exis-enquiry');


$(document).on('click','.new-enquiry',function()
{	
	$(this).addClass('active-tab');
	$('.exsisting-enquiry').removeClass('active-tab');
	new_enq.removeClass('hidden');
	ex_enq.addClass('hidden');
});

$(document).on('click','.exsisting-enquiry',function()
{
	$(this).addClass('active-tab');
	$('.new-enquiry').removeClass('active-tab');
	
	new_enq.addClass('hidden');
	ex_enq.removeClass('hidden');
});

ex_enq.submit(function(e){
	e.preventDefault();
	var x = ex_enq.serializeArray();
    var datas = new FormData();
    $.each(x, function(i, field){
    	datas.append(field.name,field.value);
    });
    $.ajax({
			url : ex_enq.attr("action"),
			type : 'post',
			data : datas,
			headers:{'X-CSRF-TOKEN': $('input[name=_token]').val()},
			contentType: false,
   			processData: false,
			beforeSend : function(){
				$('.preloader').show();
			},
			complete: function(){
				$('.preloader').hide();
			},
			success : function(response){
				if(response.status){
					var redirect_url = $('#ticket-details').data('url');
					$('.ticket-check').prop('disabled',true);
					$('#ticket-details').html(" ");
					$('.email-error strong').html(" ");
					var table = '<table style="border-radius:4px;" class="table table-hover table-bordered text-left"><thead><tr style="background:#f2f2f2;"><th>No.</th><th>Support ticket no.</th><th>Date</th><th>Actions</th></tr></thead>';
					table += '<tbody>';

					for(var i = 0; i<response.result.length;i++)
					{
						table+='<tr><td>'+(i+1)+'</td><td>'+response.result[i].ticket_no+'</td><td>'+response.result[i].created_at+'</td><td><a class="btn btn-warning btn-xs" href="'+redirect_url+"/"+response.result[i].ticket_url+'">View</a></tr>';
						//$('#ticket-details').append('<div class="row m-t-b-20"><div class="col-md-12"><div class="col-md-1"><span>'+(i+1)+'</span></div><div class="col-md-5"><label>Support ticket no.'+response.result[i].ticket_no+'</label></div><div class="col-md-5"><label>Date:</label><span>'+response.result[i].created_at+'</span></div><div class="col-md-1"><a href="'+redirect_url+"/"+response.result[i].ticket_url+'">view</a></div></div></div>');
					}
					
					table+='</tbody></table>';
					$('#ticket-details').append(table);

				} 
				else
				{
					$('.email-error strong').html(response.result.email[0]);
				}
			}
		});
});