$(document).on('click','.edit-modal-admin',function(e){
    var btn = $(e.relatedTarget);    
    var post_url = $(this).attr("data-url"); //get form action url
    var request_method = $(this).attr("data-method"); //get form GET/POST method
    var form_data = {'message_id':$(this).attr('data-id')}; 
    $('#message_id').val($(this).attr('data-id'));
    getMessage(post_url,request_method,form_data,function(res){
        $('.list-body').html("");
        $('.modal-title').html("subject:"+res.messages[0].subject)
        var main = [{type:"div",class:"msg_history"}];
        gridLayout(main,function(maindiv){
            $('.list-body').append(maindiv[0]);
            if(res.messages[0].created_by == 'admin'){
                var out_main = [{type:"div",class:"outgoing_msg"}];
                gridLayout(out_main,function(out_mains){
                    maindiv[0].appendChild(out_mains[0]);
                    var sent_msg = [{type:"div",class:"sent_msg"}];
                    gridLayout(sent_msg,function(sent_msgs){
                        out_mains[0].appendChild(sent_msgs[0]);
                        var text_msg = [{type:"p",text:res.messages[0].message}];
                        gridLayout(text_msg,function(text_msgs){
                            sent_msgs[0].appendChild(text_msgs[0]);
                        });

                        var datess = res.messages[0].time + res.messages[0].session+"  |  "+res.messages[0].date;
                        var date_and_time = [{type:"span",class:"time_date",text:datess}]
                        gridLayout(date_and_time,function(date_and_times){
                            sent_msgs[0].appendChild(date_and_times[0]);
                        });
                    });
                });
            } else{
                var income_msg = [{type:'div',class:'incoming_msg'}]
                gridLayout(income_msg,function(income_msgs){
                    maindiv[0].appendChild(income_msgs[0]);
                    var incoming_msg_img = [{type:'div',class:'incoming_msg_img'}];
                    gridLayout(incoming_msg_img,function(incoming_msg_imgs){
                        income_msgs[0].appendChild(incoming_msg_imgs[0]);
                        var income_img = "<img src=\"https://ptetutorials.com/images/user-profile.png\" alt=\"sunil\">";
                        income_img = $.parseHTML(income_img)
                        $(incoming_msg_imgs).append(income_img);
                    });
                    var received_msg = [{type:'div',class:'received_msg'}];
                    gridLayout(received_msg,function(received_msgs){
                        income_msgs[0].appendChild(received_msgs[0]);
                        var received_withd_msg = [{type:'div',class:'received_withd_msg'}];
                        gridLayout(received_withd_msg,function(received_withd_msgs){
                            received_msgs[0].appendChild(received_withd_msgs[0]);
                            var received_msgs_text = [{type:'p',text:res.messages[0].message}];
                            gridLayout(received_msgs_text,function(received_msgs_texts){
                                received_withd_msgs[0].appendChild(received_msgs_texts[0]);
                            });
                            var datess = res.messages[0].time + res.messages[0].session+"  |  "+res.messages[0].date;
                            var date_and_times = [{type:"span",class:"time_date",text:datess}]
                            gridLayout(date_and_times,function(date_and_timess){
                                received_withd_msgs[0].appendChild(date_and_timess[0]);
                            });
                        });
                    });
                });
            }
            $.each(res.messages[0].conversations,function(con_key,con_value){
                if(con_value.created_by == 'user'){
                    var income_msg = [{type:'div',class:'incoming_msg'}]
                    gridLayout(income_msg,function(income_msgs){
                        maindiv[0].appendChild(income_msgs[0]);
                        var incoming_msg_img = [{type:'div',class:'incoming_msg_img'}];
                        gridLayout(incoming_msg_img,function(incoming_msg_imgs){
                            income_msgs[0].appendChild(incoming_msg_imgs[0]);
                            var income_img = "<img src=\"https://ptetutorials.com/images/user-profile.png\" alt=\"sunil\">";
                            income_img = $.parseHTML(income_img)
                            $(incoming_msg_imgs).append(income_img);
                        });
                        var received_msg = [{type:'div',class:'received_msg'}];
                        gridLayout(received_msg,function(received_msgs){
                            income_msgs[0].appendChild(received_msgs[0]);
                            var received_withd_msg = [{type:'div',class:'received_withd_msg'}];
                            gridLayout(received_withd_msg,function(received_withd_msgs){
                                received_msgs[0].appendChild(received_withd_msgs[0]);
                                var received_msgs_text = [{type:'p',text:con_value.messages}];
                                gridLayout(received_msgs_text,function(received_msgs_texts){
                                    received_withd_msgs[0].appendChild(received_msgs_texts[0]);
                                });
                                if(con_value.image_path){
                                    var income_img = "<a href='"+con_value.image_path+"' download alt=\"sunil\" target=\"_blank\">download</a>";
                                    income_img = $.parseHTML(income_img)
                                    $(received_withd_msgs).append(income_img);
                                }
                                var datess = con_value.time + con_value.session+"  |  "+con_value.date;
                                var date_and_times = [{type:"span",class:"time_date",text:datess}]
                                gridLayout(date_and_times,function(date_and_timess){
                                    received_withd_msgs[0].appendChild(date_and_timess[0]);
                                });
                            });
                        });
                    });
                } else{

                    var out_main = [{type:"div",class:"outgoing_msg"}];
                    gridLayout(out_main,function(out_mains){
                        maindiv[0].appendChild(out_mains[0]);
                        var sent_msg = [{type:"div",class:"sent_msg"}];
                        gridLayout(sent_msg,function(sent_msgs){
                            out_mains[0].appendChild(sent_msgs[0]);
                            var text_msg = [{type:"p",text:con_value.messages}];
                            gridLayout(text_msg,function(text_msgs){
                                sent_msgs[0].appendChild(text_msgs[0]);
                            });
                            if(con_value.image_path){
                                var income_img = "<a href='"+con_value.image_path+"' download alt=\"sunil\" target=\"_blank\">download</a>";
                                income_img = $.parseHTML(income_img)
                                $(sent_msgs).append(income_img);
                            }
                            var datess = con_value.time + con_value.session+"  |  "+con_value.date;
                            var date_and_time = [{type:"span",class:"time_date",text:datess}]
                            gridLayout(date_and_time,function(date_and_times){
                                sent_msgs[0].appendChild(date_and_times[0]);
                            });
                        });
                    });
                }
            });
            var outgoing_new_msg = [{type:'div',class:'outgoing_new_msg'}];
            gridLayout(outgoing_new_msg,function(outgoing_new_msgs){
                maindiv[0].appendChild(outgoing_new_msgs[0]);
            });
        });
    });
});

$(document).on('click','.edit-modal',function(e){
    var btn = $(e.relatedTarget);
    var post_url = $(this).attr("data-url"); //get form action url
    var request_method = $(this).attr("data-method"); //get form GET/POST method
    var form_data = {'message_id':$(this).attr('data-id')}; 
    $('#message_id').val($(this).attr('data-id'));
    getMessage(post_url,request_method,form_data,function(res){
        $('.list-body').html("");
        $('.modal-title').html("subject:"+res.messages[0].subject)
        var main = [{type:"div",class:"msg_history"}];
        gridLayout(main,function(maindiv){
            $('.list-body').append(maindiv[0]);
            if(res.messages[0].created_by == 'user'){
                var out_main = [{type:"div",class:"outgoing_msg"}];
                gridLayout(out_main,function(out_mains){
                    maindiv[0].appendChild(out_mains[0]);
                    var sent_msg = [{type:"div",class:"sent_msg"}];
                    gridLayout(sent_msg,function(sent_msgs){
                        out_mains[0].appendChild(sent_msgs[0]);
                        var text_msg = [{type:"p",text:res.messages[0].message}];
                        gridLayout(text_msg,function(text_msgs){
                            sent_msgs[0].appendChild(text_msgs[0]);
                        });
                        var datess = res.messages[0].time + res.messages[0].session+"  |  "+res.messages[0].date;
                        var date_and_time = [{type:"span",class:"time_date",text:datess}]
                        gridLayout(date_and_time,function(date_and_times){
                            sent_msgs[0].appendChild(date_and_times[0]);
                        });
                    });
                });
            } else{
                var income_msg = [{type:'div',class:'incoming_msg'}]
                gridLayout(income_msg,function(income_msgs){
                    maindiv[0].appendChild(income_msgs[0]);
                    var incoming_msg_img = [{type:'div',class:'incoming_msg_img'}];
                    gridLayout(incoming_msg_img,function(incoming_msg_imgs){
                        income_msgs[0].appendChild(incoming_msg_imgs[0]);
                        var income_img = "<img src=\"https://ptetutorials.com/images/user-profile.png\" alt=\"sunil\">";
                        income_img = $.parseHTML(income_img)
                        $(incoming_msg_imgs).append(income_img);
                    });
                    var received_msg = [{type:'div',class:'received_msg'}];
                    gridLayout(received_msg,function(received_msgs){
                        income_msgs[0].appendChild(received_msgs[0]);
                        var received_withd_msg = [{type:'div',class:'received_withd_msg'}];
                        gridLayout(received_withd_msg,function(received_withd_msgs){
                            received_msgs[0].appendChild(received_withd_msgs[0]);
                            var received_msgs_text = [{type:'p',text:res.messages[0].message}];
                            gridLayout(received_msgs_text,function(received_msgs_texts){
                                received_withd_msgs[0].appendChild(received_msgs_texts[0]);
                            });
                            var datess = res.messages[0].time + res.messages[0].session+"  |  "+res.messages[0].date;
                            var date_and_times = [{type:"span",class:"time_date",text:datess}]
                            gridLayout(date_and_times,function(date_and_timess){
                                received_withd_msgs[0].appendChild(date_and_timess[0]);
                            });
                        });
                    });
                });
            }
            $.each(res.messages[0].conversations,function(con_key,con_value){
                if(con_value.created_by == 'admin'){
                    var income_msg = [{type:'div',class:'incoming_msg'}]
                    gridLayout(income_msg,function(income_msgs){
                        maindiv[0].appendChild(income_msgs[0]);
                        var incoming_msg_img = [{type:'div',class:'incoming_msg_img'}];
                        gridLayout(incoming_msg_img,function(incoming_msg_imgs){
                            income_msgs[0].appendChild(incoming_msg_imgs[0]);
                            var income_img = "<img src=\"https://ptetutorials.com/images/user-profile.png\" alt=\"sunil\">";
                            income_img = $.parseHTML(income_img)
                            $(incoming_msg_imgs).append(income_img);
                        });
                        var received_msg = [{type:'div',class:'received_msg'}];
                        gridLayout(received_msg,function(received_msgs){
                            income_msgs[0].appendChild(received_msgs[0]);
                            var received_withd_msg = [{type:'div',class:'received_withd_msg'}];
                            gridLayout(received_withd_msg,function(received_withd_msgs){
                                received_msgs[0].appendChild(received_withd_msgs[0]);
                                var received_msgs_text = [{type:'p',text:con_value.messages}];
                                gridLayout(received_msgs_text,function(received_msgs_texts){
                                    received_withd_msgs[0].appendChild(received_msgs_texts[0]);
                                });
                                if(con_value.image_path){
                                    var income_img = "<a href='"+con_value.image_path+"' download alt=\"sunil\" target=\"_blank\">download</a>";
                                    income_img = $.parseHTML(income_img)
                                    $(received_withd_msgs).append(income_img);
                                }
                                var datess = con_value.time + con_value.session+"  |  "+con_value.date;
                                var date_and_times = [{type:"span",class:"time_date",text:datess}]
                                gridLayout(date_and_times,function(date_and_timess){
                                    received_withd_msgs[0].appendChild(date_and_timess[0]);
                                });
                            });
                        });
                    });
                } else{

                    var out_main = [{type:"div",class:"outgoing_msg"}];
                    gridLayout(out_main,function(out_mains){
                        maindiv[0].appendChild(out_mains[0]);
                        var sent_msg = [{type:"div",class:"sent_msg"}];
                        gridLayout(sent_msg,function(sent_msgs){
                            out_mains[0].appendChild(sent_msgs[0]);
                            var text_msg = [{type:"p",text:con_value.messages}];
                            gridLayout(text_msg,function(text_msgs){
                                sent_msgs[0].appendChild(text_msgs[0]);
                            });
                            if(con_value.image_path){
                                var income_img = "<a href='"+con_value.image_path+"' download alt=\"sunil\" target=\"_blank\">download</a>";
                                income_img = $.parseHTML(income_img)
                                $(sent_msgs).append(income_img);
                            }
                            var datess = con_value.time + con_value.session+"  |  "+con_value.date;
                            var date_and_time = [{type:"span",class:"time_date",text:datess}]
                            gridLayout(date_and_time,function(date_and_times){
                                sent_msgs[0].appendChild(date_and_times[0]);
                            });
                        });
                    });
                }
            });
            var outgoing_new_msg = [{type:'div',class:'outgoing_new_msg'}];
            gridLayout(outgoing_new_msg,function(outgoing_new_msgs){
                maindiv[0].appendChild(outgoing_new_msgs[0]);
            });
        });
    });
});

var gridLayout = function(item,cb){
        var elements=[];
        for(var i =0; i<item.length; i++){
            var  element = document.createElement(item[i].type);
            if(item[i].class) element.setAttribute('class',item[i].class);
            if(item[i].id) element.setAttribute('id',item[i].id);
            if(item[i].types) element.setAttribute('type',item[i].types);
            if(item[i].value) element.setAttribute('value',item[i].value);
            if(item[i].for) element.setAttribute('for',item[i].for);
            if(item[i].relation) element.setAttribute('relation',item[i].relation);
            if(item[i].extra) element.setAttribute('extra',item[i].extra);
            if(item[i].checked) element.setAttribute('checked',item[i].checked);
            if(item[i].style) element.setAttribute('style',item[i].style);
            if(item[i].text) element.innerHTML = item[i].text;
            elements.push(element);
        }
        cb(elements);
    };

$("#chat_form").submit(function(event){
    event.preventDefault();
    var post_url = $(this).attr("action"); //get form action url
    var request_method = $(this).attr("method"); //get form GET/POST method
    var form_data = new FormData($(this)[0]); //Encode form elements for submission
    form_data.append('reply_message', CKEDITOR.instances.write_msg.getData());
    replyMessage(post_url,request_method,form_data,function(res){
        if(res.status == "error"){
            if(res.messages.reply_message){
                $('.email_tex').removeClass('hidden');
                $('.error_text').text(res.messages.reply_message[0]);
            } else{
                $('.email_tex').addClass('hidden');
            }   
        } else{
            $('.email_tex').addClass('hidden');
            var attachments = '<a href="'+res.messages.image_path+'" download alt=\"sunil\" target=\"_blank\">download</a>';
            if(res.messages.image_path){
                var new_message = '<div class="outgoing_msg"><div class="sent_msg"><p>'+res.messages.messages+'</p>'+attachments+'<span class="time_date">'+res.messages.time+' '+res.messages.session+'    |    '+res.messages.date+'</span> </div></div>';
            } else{
                var new_message = '<div class="outgoing_msg"><div class="sent_msg"><p>'+res.messages.messages+'</p><span class="time_date">'+res.messages.time+' '+res.messages.session+'    |    '+res.messages.date+'</span> </div></div>';
            }
            
            $('.outgoing_new_msg').append(new_message);
            $('#write_msg').val("");
            $('.attachments_').val("");
            CKEDITOR.instances.write_msg.setData('');
             $('.msg_history').animate({
                 scrollTop: $(".outgoing_new_msg").offset().top
             }, 1500);
        }
    });
});

function getMessage(post_url,request_method,form_data,cb) {
    $.ajax({
        url : post_url,
        type: request_method,
        data : form_data,
        enctype: 'multipart/form-data',
    }).done(function(response){ //
         cb(response);
         //setTimeout(function () { $('.edit-modal-admin').click() }, 5000);
    });
}
function replyMessage(post_url,request_method,form_data,cb) {
    $.ajax({
        url : post_url,
        cache: false,
        contentType: false,
        processData: false,
        type: request_method,
        data : form_data,
        enctype: 'multipart/form-data',
    }).done(function(response){ //
         cb(response);
    });
}