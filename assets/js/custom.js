$('#select_all').change(function() {
    var checkboxes = $('.tbl_content').find(':checkbox');
    checkboxes.prop('checked', $(this).is(':checked'));
});

var window_height = $(window).height();
var document_height = $(document).height();

//alert('Window : '+window_height+', Document : '+document_height);

if(window_height>=document_height)
{
    $('.thankyou').css("min-height", ((window_height)-225)+"px");
    $('#message-table-container').css("min-height", ((window_height)-265)+"px");
    
}